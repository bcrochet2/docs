= Access Recovery

If you've lost the private key of an SSH key pair used to log into a Fedora system, and do not have any password logins set up to use at the console, you can gain access back to the machine with the following steps:

. When booting the system, intercept the GRUB menu and edit the entry to append `init=/bin/bash``
. Wait for the system to boot into a shell prompt

Execute the following commands:

- `mount -t selinuxfs selinuxfs /sys/fs/selinux`
- `/sbin/load_policy`
- `passwd root`
- `sync`
- `/sbin/reboot -ff`

Or alternatively, you can manually attempt to update e.g. `~/.ssh/authorized_keys`.

Note that Fedora by default does not allow SSH login via password authentication.
